async function getSuggestions() {
    return fetch('api/suggestions', {
        method: 'GET'
    })
}

async function joinSuggestion(suggestionId, userEmail){
    return fetch(`api/suggestions/join/${suggestionId}`, {
        method: 'POST',
        body: JSON.stringify({userEmail})
    })
}

export {
    getSuggestions, joinSuggestion
}