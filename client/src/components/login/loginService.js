async function login(user_email) {
    return fetch('api/login', {
        method: 'POST',
        body: JSON.stringify({user_email})
    })
}

export {
    login
}