async function getProfiles() {
    return fetch('api/profiles', {
        method: 'GET'
    })
}

export {
    getProfiles
}