import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';
import VueCookies from 'vue3-cookies'
import InputText from 'primevue/inputtext'
import Card from "primevue/card";
import Button from "primevue/button";
import Carousel from 'primevue/carousel';
import Dropdown from 'primevue/dropdown';
import DataViewLayoutOptions from 'primevue/dataviewlayoutoptions';
import DataView from 'primevue/dataview'
import Avatar from 'primevue/avatar';
import AvatarGroup from 'primevue/avatargroup';
import Badge from 'primevue/badge';
import ProgressSpinner from 'primevue/progressspinner';

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';



const app = createApp(App)

app.use(router)
app.use(PrimeVue);
app.use(VueCookies, {
    expireTimes: "7d",
    secure: true,
});
app.component('InputText', InputText)
app.component('Card', Card)
app.component('Button', Button)
app.component('Carousel', Carousel)
app.component('Dropdown', Dropdown);
app.component('DataViewLayoutOptions', DataViewLayoutOptions);
app.component('DataView', DataView);
app.component('Avatar', Avatar);
app.component('Badge', Badge);
app.component('AvatarGroup', AvatarGroup);
app.component('ProgressSpinner', ProgressSpinner)

app.mount('#app')
