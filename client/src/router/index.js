import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import SuggestionsView from '../views/SuggestionsView.vue';
import SuggestionCreationView from '../views/SuggestionCreationView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/suggestions',
      name: 'suggestions',
      component: SuggestionsView
    },
    {
      path: '/suggestions/new',
      name: 'suggestion-creation',
      component: SuggestionCreationView
    },
    {
      path: '/:pathMatch(.*)*',
      redirect: 'login'
    }
  ]
})

export default router
