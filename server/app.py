import datetime
import json
import uuid
from pathlib import Path

import asyncpg
from asyncpg import Pool, Connection
from flask import Flask, jsonify, send_from_directory, request, g

STATIC_FILES_PATH = '../client/dist'
POSTGRES_CONNECTION_STRING = "postgres://postgres:blabla123@ec2-18-205-113-1.compute-1.amazonaws.com/cibus"
app = Flask(__name__)


@app.route('/api/suggestions', methods=['GET'])
async def get_suggestions():
    suggestions = json.loads(Path("./suggestions.json").read_bytes())

    return jsonify([suggestion for suggestion in suggestions])


def _validate_login_info(login_info: dict):
    if "user_email" not in login_info or not (
            login_info["user_email"].endswith("cynerio.co") or login_info["user_email"].endswith("cynerio.com")):
        raise ValueError("Login information must contain valid Cynerio's user email.")


@app.route('/api/login', methods=['POST'])
async def login():
    connection = await asyncpg.connect(dsn=POSTGRES_CONNECTION_STRING)

    try:
        login_info = json.loads(request.data)
        _validate_login_info(login_info)
        connection.execute("INSERT INTO profiles(profile_id, email) VALUES ($1, $2) ON CONFLICT DO NOTHING",
                           uuid.uuid4(),
                           login_info["user_email"])

        return jsonify(success=True)
    finally:
        await connection.close()


@app.route('/api/suggestions/join/<suggestion_id>', methods=['POST'])
async def join_suggestion_group(suggestion_id):
    connection = await asyncpg.connect(dsn=POSTGRES_CONNECTION_STRING)

    try:
        user_email = json.loads(request.data)["userEmail"]
        await connection.execute("UPDATE profiles SET joined_suggestion=$1, joined_time=NOW() WHERE email=$2",
                                 suggestion_id, user_email)

        return jsonify(success=True)
    finally:
        await connection.close()


@app.route('/api/profiles', methods=['GET'])
async def get_profiles():
    connection = await asyncpg.connect(dsn=POSTGRES_CONNECTION_STRING)

    try:
        profiles = [{"userEmail": raw_profile["email"], "joinedSuggestionId": raw_profile["joined_suggestion"],
                         "joinedTime": raw_profile["joined_time"], "avatarUrl": raw_profile["avatar_url"]} for raw_profile
                        in
                        await connection.fetch(
                            "SELECT email, joined_suggestion, joined_time,"
                            " avatar_url FROM profiles")]

        return jsonify(profiles)
    finally:
        await connection.close()


@app.route('/assets/<path>', methods=['GET'])
async def get_static_assets(path):
    return send_from_directory(STATIC_FILES_PATH + "/assets", path)


@app.route('/', methods=['GET'])
async def get_compiled_page():
    return send_from_directory(STATIC_FILES_PATH, 'index.html')


@app.errorhandler(404)
def handle_not_found(error):
    return send_from_directory(STATIC_FILES_PATH, 'index.html')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
